ARG PYTHON_VERSION=3.10.12
FROM python:${PYTHON_VERSION}-bullseye

LABEL maintainer.name="CMS Data Preservation and Open Access Group"
LABEL maintainer.email="cms-dpoa-coordinators@cern.ch"

ENV LANG=C.UTF-8

ARG PLATFORM="linux/amd64"
ARG PYTHON_VERSION=3.10.12

WORKDIR /usr/local

SHELL [ "/bin/bash", "-c" ]

COPY packages.txt packages.txt
COPY requirements.txt requirements.txt

# Set PATH to pickup virtualenv by default
ENV PATH=/usr/local/venv/bin:"${PATH}"
RUN apt-get -qq -y update && \
    DEBIAN_FRONTEND=noninteractive apt-get -qq -y install --no-install-recommends $(cat packages.txt) && \
    apt-get -y autoclean && \
    apt-get -y autoremove && \
    rm -rf /var/lib/apt/lists/* && \
    python -m venv /usr/local/venv && \
    . /usr/local/venv/bin/activate && \
    python -m pip --no-cache-dir install --upgrade pip setuptools wheel && \
    python -m pip --no-cache-dir install --requirement requirements.txt && \
    python -m pip list && \
    ln --symbolic --force /bin/bash /bin/sh && \
    ln -sf /usr/share/zoneinfo/UTC /etc/localtime

# Add XRootD under /usr/local
ARG XROOTD_TAG=v5.5.5
WORKDIR /code
RUN git clone --depth 1 https://github.com/xrootd/xrootd.git \
        --branch "${XROOTD_TAG}" \
        --single-branch \
        xrootd \
    && cmake \
        -DCMAKE_INSTALL_PREFIX=/usr/local/venv \
        -DPIP_VERBOSE=ON \
        -S xrootd \
        -B build \
    && cmake build -LH \
    && cmake \
        --build build \
        --clean-first \
        --parallel $(($(nproc) - 1)) \
    && cmake --build build --target install \
    && cd / \
    && rm -rf /code

RUN echo /usr/local/venv/lib >> /etc/ld.so.conf \
    && ldconfig

RUN git clone --depth 1 --single-branch https://github.com/novnc/noVNC.git /usr/local/novnc

RUN groupadd -g 1000 cmsusr && adduser --uid 1000 --gid 1000 cmsusr --disabled-password && \
    usermod -aG sudo cmsusr && \
    chown -R cmsusr:cmsusr /usr/local/venv

RUN echo "source /usr/local/vnc_utils.sh" >> /home/cmsusr/.bashrc && \
    echo "source /usr/local/vnc_utils.sh" >> /home/cmsusr/.zshrc && \
    printf '\nexport PATH=/usr/local/venv/bin:"${PATH}"\n' >> /home/cmsusr/.bashrc

COPY  vnc/vnc_utils.sh /usr/local/vnc_utils.sh

# Use C.UTF-8 locale to avoid issues with ASCII encoding
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

RUN mkdir /code && \
    chown -R cmsusr:cmsusr /code && \
    chmod 777 /code

WORKDIR /code
USER cmsusr
ENV USER cmsusr
ENV HOME /home/cmsusr
ENV GEOMETRY 1920x1080

RUN mkdir -p ${HOME}/.vnc
ADD --chown=cmsusr:cmsusr vnc/passwd ${HOME}/.vnc/passwd

ENTRYPOINT ["/bin/bash", "-l", "-c"]
CMD ["/bin/bash"]

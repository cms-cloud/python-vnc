default: python_image

all: python_image

# global build variables
PYTHON_VERSION=3.10.12
PLATFORM="linux/amd64"

python_image:
	docker build . \
	-f Dockerfile \
	--platform "${PLATFORM}" \
	--build-arg PLATFORM=${PLATFORM} \
	--build-arg XROOTD_TAG=v5.5.5 \
	--build-arg PYTHON_VERSION=$(PYTHON_VERSION) \
	--tag cmscloud/python-vnc:python$(PYTHON_VERSION) \
	--tag cmscloud/python-vnc


run_python_image:
	docker run --rm -it cmscloud/python-vnc:latest

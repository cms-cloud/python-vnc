# Python HEP VNC

Python image with HEP software stack and a VNC server

[![pipeline status](https://gitlab.cern.ch/cms-cloud/python-vnc/badges/main/pipeline.svg)](https://gitlab.cern.ch/cms-cloud/python-vnc/commits/main)

The Docker images contain Python 3 and a VNC server for access to the graphical user interface.

## Usage

```shell
docker run --rm -it -P -p 5901:5901 -p 6080:6080 -p 8888:8888 gitlab-registry.cern.ch/cms-cloud/python-vnc:latest
```

If you would like to mount directories from your local file system into the container, please use `/code` as target directory in the container (and in particular do not mount to `/home/cmsusr` since this will break the VNC setup).

To use [Jupyter Lab](https://jupyter.org/), run `jupyter-lab --ip=0.0.0.0 --no-browser` and then point your browser to [http://localhost:8888/lab](http://localhost:8888/lab).

To start the VNC server when not using Jupyter Lab, run in the container:

```shell
start_vnc
```

The VNC server will be available on `localhost:5901` and also be accessible via a webbrowser at [http://localhost:6080]([http://localhost:6080]). The connection password is `cms.cern`.
